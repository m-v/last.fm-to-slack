# LastFM to Slack

To get this running do:

```bash
npm ci
cp .env.example .env
# go fill out the .env file
npm start
```

Once running it will check your LastFM profile if you played any song in last 15 minutes.
If yes it checks your slack status.
If you're online, and you don't have any custom status, it sets one  for you to `🎵 Listening to "<artist> - <song>"`.

## LastFM key

You'll need to register a LastFM app. Go to https://www.last.fm/api/account/create and make one. You'll get a 32 character key, the secret isn't used.

## Slack Key

You need a slack app, in a paid space. Go to https://api.slack.com/apps/new to create a new app.
* Make a new app and go to "OAuth and Permissions" from the sidebar on the left under the "Features" menu section.
* Scroll down until you see "User token scopes" and click "Add an OAuth scope"
* Type in `users.profile:write` and select it from the menu, repeat for `users.profile:read` and `users:read`
* Scroll back to the top and click the "Install App to Workspace" button. On the next screen click "Allow"
* You'll get an access token starting with `xoxp-` 🎉
