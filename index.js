import 'dotenv/config'
import term from 'terminal-kit';
import fetch from 'node-fetch';

const UPDATE_INTERVAL_MS = 30000;
const AUTOPAUSE_TRIGGER_MIN = 15;
const STATUS_EMOJI = ':musical_note:';
const STATUS_LINKED = '✓ Slack status is linked';
const STATUS_PAUSED = '⏹ Slack status is free';
const TITLE_LINKED = '♫ Last.fm to Slack ♫';
const TITLE_PAUSED = '♫ (paused) Last.fm to Slack  ♫';
const HELP_MESSAGE = 'Press any key to pause/resume updating slack status, or ctrl+c to close the app.';

const { terminal } = term;
const { LASTFM_USERNAME, LASTFM_KEY, LASTFM_SECRET, SLACK_TOKEN } = process.env;

const recentUrl = `http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=${encodeURIComponent(LASTFM_USERNAME)}&api_key=${LASTFM_KEY}&format=json&limit=2`;
const getLastSong = async () => {
	try {
		const fromSec = Math.floor((Date.now() - (AUTOPAUSE_TRIGGER_MIN * 60000)) / 1000);
		const response = await fetch(recentUrl + `&from=${fromSec}`, {
			headers: { 'User-Agent': 'Slack syncer' }
		});
		const json = await response.json();
		const track = json.recenttracks?.track.length !== undefined ? json.recenttracks?.track[0] : json.recenttracks?.track;
		if (!track) return;
		const trackInfo = `${track.artist['#text']} - ${track.name}`;
		return trackInfo;
	} catch (e) {
		if (e.code !=='EAI_AGAIN') { // just ignore these
			console.error(e);
		}
	}
}

const getCurrentStatus = async () => {
	try {
		const responsePresence = await fetch('https://slack.com/api/users.getPresence', {
			headers: {
				Authorization: `Bearer ${SLACK_TOKEN}`,
			}
		});
		const jsonPresence = await responsePresence.json();
		if (!jsonPresence.online) return;

		const responseProfile = await fetch('https://slack.com/api/users.profile.get', {
			headers: {
				Authorization: `Bearer ${SLACK_TOKEN}`,
			}
		});
		const jsonProfile = await responseProfile.json();
		return {
			status_emoji: jsonProfile.profile.status_emoji,
			status_text: jsonProfile.profile.status_text,
		}
	} catch (e) {
		if (e.code ==='EAI_AGAIN') {
			return getCurrentStatus();
		} else {
			console.error(e);
		}
	}
}

const setStatus = async (song) => {
	try {
		const r = await fetch('https://slack.com/api/users.profile.set', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${SLACK_TOKEN}`,
				'Content-type': 'application/json',
			},
			body: JSON.stringify({
				profile: {
					'status_text': song ? `Listening to "${song}"` : '',
					'status_emoji': song ? STATUS_EMOJI : '',
				}
			}),
		});
		const json = await r.json();
		return json.ok;
	} catch (e) {
		if (e.code ==='EAI_AGAIN') {
			return setStatus(song);
		} else {
			console.error(e);
		}
	}
}

(async () => {
	terminal.windowTitle(TITLE_LINKED);
	const document = terminal.createDocument({
		palette: new term.Palette(),
	});

	const lastfmSpinner = await terminal.moveTo(29, 2).spinner();
	lastfmSpinner.animate(2);
	lastfmSpinner.show();
	const slackSpinner = await terminal.moveTo(29, 3).spinner();
	slackSpinner.animate(2);
	slackSpinner.show();

	const statusLinked = new term.Text({
		parent: document,
		content: STATUS_LINKED,
		attr: { bgColor: 'black', color: 'green' },
		x: 0,
		y: 0,
		hidden: false,
	});
	const statusPaused = new term.Text({
		parent: document,
		content: STATUS_PAUSED,
		attr: { bgColor: 'black', color: 'red' },
		x: 0,
		y: 0,
		hidden: true,
	});
	const text = new term.Text({
		parent: document,
		content: 'Last.fm recheck in: ',
		attr: { bgColor: 'black' },
		x: 30,
		y: 0,
	});
	const timeToUpdate = new term.Text({
		parent: document,
		content: '15.0s',
		attr: { bgColor: 'black' },
		x: 50,
		y: 0,
	});
	const text2 = new term.Text({
		parent: document,
		content: 'Currently playing:',
		attr: { bgColor: 'black' },
		x: 0,
		y: 1,
	});
	const songInfo = new term.Text({
		parent: document,
		content: '',
		attr: { bgColor: 'black' },
		x: 30,
		y: 1,
	});
	const text3 = new term.Text({
		parent: document,
		content: 'Slack status:',
		attr: { bgColor: 'black' },
		x: 0,
		y: 2,
	});
	const slackStatus = new term.Text({
		parent: document,
		content: '',
		attr: { bgColor: 'black' },
		x: 30,
		y: 2,
	});
	const log = [];
	const text4log = new term.TextBox({
		parent: document,
		content: [...log, HELP_MESSAGE],
		attr: { bgColor: 'black', color: 'gray' },
		x: 0,
		y: 3,
		width: 120,
		height: 10,
	});
	const logMessage = (message) => {
		log.push(`${new Date().toLocaleTimeString()}: ${message}`);
		text4log.setContent([...log.slice(-5), HELP_MESSAGE].join('\n'));
	};
	
	let lastSong = '';
	let timeLeftMs = UPDATE_INTERVAL_MS;
	let paused = false;
	let songStarted = Date.now();
	const updateSong = async () => {
		timeLeftMs = UPDATE_INTERVAL_MS;
		lastfmSpinner.show();
		const song = await getLastSong();
		lastfmSpinner.hide();
		if (song !== lastSong) {
			const minsSince = (Date.now() - songStarted) / 60000;
			songInfo.setContent(song ?? '-nothing-');
			if (!song && minsSince < AUTOPAUSE_TRIGGER_MIN) {
				logMessage(`Most likely connection fail (${Math.floor(minsSince)}). Last song: ${lastSong}`);
				return;
			} else if (!song && lastSong) {
				logMessage(`Got no songs. Last song: ${lastSong}`);
			}
			updateStatus(song);
			if (song) {
				songStarted = Date.now();
			}
			lastSong = song;
		}
	}

	const updateStatus = async (newSong) => {
		slackSpinner.show();
		const status = await getCurrentStatus();
		if (status) { // online
			if (!status.status_emoji || status.status_emoji === STATUS_EMOJI) {
				const done = await setStatus(newSong);
				if (done) {
					slackStatus.setContent(newSong ? `${STATUS_EMOJI} Listening to "${newSong}"` : '-online-');
					slackSpinner.hide();
					return;
				}
			} else {
				logMessage(`Status "${status.status_emoji} ${status.status_text}" found, "${newSong ?? 'online'}" not set.`)
			}
		}
		slackStatus.setContent(status ? `${status.status_emoji} ${status.status_text}` : '-offline-');
		slackSpinner.hide();
	}

	const updatePause = async (newPaused) => {
		paused = newPaused;
		if (paused) {
			stop();
			statusLinked.hide()
			statusPaused.show()
			terminal.windowTitle(TITLE_PAUSED);
			timeToUpdate.setContent('-never-');
			lastfmSpinner.hide();
			slackSpinner.show();
			await updateStatus()
			slackSpinner.hide();
			songInfo.setContent('-untracked-');
			slackStatus.setContent('-online-');
		} else {
			start();
			statusLinked.show()
			statusPaused.hide()
			terminal.windowTitle(TITLE_LINKED);
		}
		terminal.moveTo(0, 10).hideCursor();
	}

	let lastfmInterval;
	let timeUpdateInterval;
	const start = () => {
		stop(); // just in case
		updateSong();
		lastfmInterval = setInterval(updateSong, UPDATE_INTERVAL_MS);
		timeUpdateInterval = setInterval(() => {
			timeLeftMs -= 100;
			timeToUpdate.setContent(`${(timeLeftMs / 1000).toFixed(1)}s`);
			terminal.moveTo(0, 10).hideCursor();
		}, 100);
	};
	const stop = () => {
		lastSong = '';
		clearInterval(lastfmInterval);
		clearInterval(timeUpdateInterval);
	};
	start();

	terminal.on('key', (key) => {
		if (key === 'CTRL_C') {
			terminal.grabInput(false);
			terminal.hideCursor(false);
			terminal.styleReset();
			// terminal.clear();
			process.exit();
		} else {
			updatePause(!paused);
		}
	});
})();
